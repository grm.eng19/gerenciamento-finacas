from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
# resquest -> response
# request handler
# action
#  different from a template

def home(request):
    return render(request, 'home.html')

def perfil(request):
    return render(request, 'perfil.html')

def despesas(request):
    return render(request, 'despesas.html')

def descobrir_perfil(request):
    return render(request, 'descobrir_perfil.html')

def cadastrar(request):
    return render(request, 'cadastrar.html')

def adicionar_despesa(request):
    return render(request, 'adicionar_despesa.html')

def editar_perfil(request):
    return render(request, 'editar_perfil.html')

