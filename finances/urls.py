from django.urls import path
from . import views

# URLConf
urlpatterns = [
    path('', views.home, name='home'),
    path('perfil/', views.perfil, name='perfil'),
    path('despesas/', views.despesas, name='despesas'),
    path('descobrir_perfil/', views.descobrir_perfil, name='descobrir_perfil'),
    path('cadastrar/', views.cadastrar, name='cadastrar'),
    path('adicionar_despesa/', views.adicionar_despesa, name='adicionar_despesa'),
    path('editar_perfil/', views.editar_perfil, name='editar_perfil'),

]
