from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
# resquest -> response
# request handler
# action
#  different from a template


def say_hello(request):
    return render(request, 'hello.html', {'name': 'Gabi'})
